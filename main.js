// 1 Définir le tableau
const tasks = [
    { title: 'Faire les courses', isComplete: false },
    { title: 'Nettoyer la maison', isComplete: true },
    { title: 'Planter le jardin', isComplete: false }
  ];
  
// 2 Déclaration de la nouvelle tâche
const newTask = {title: 'Réparer le chat', isComplete: true};

    // Création de la f() addTask qui ajoute une tâche avec push()
    const addTaskPush = (array, element) => {
        array.push(element);
    };

    // Création de la f() addTask qui ajoute une tâche avec spread
    const addTask = (arrays, element) => {
        return [...arrays, element];
    };

    // Appel de la fonction addtask
    const allTasks = addTask(tasks, newTask);

    // Vérification de la fonction addTask
    console.log('Add a task :', allTasks)

// 3 Création de la fonction removeTask
const removeTask = (arrays) => {
    return arrays.filter(array => {
        return array.isComplete == true;
    });
}

    // Appel de la fonction removeTask avec le tableau précédemment créé
    console.log('Remove a task :', removeTask(tasks));

// 3bis Création de la f() toggleTaskStatus

const toggleTaskStatus = allTasks.map((eachTask) => {
    if (eachTask.isComplete == true) {
        eachTask.isComplete = false;
    } else {
        eachTask.isComplete = true;
    }
    return eachTask
})
console.log('Toggle the status:', toggleTaskStatus)

// 4 Afficher les istes de tâches

    // Afficher toutes les tâches 
    const allTasksOfTheArray = allTasks.filter((eachTask) => {
        return eachTask;
    });
    console.log('All the tasks :', allTasksOfTheArray)

    // Afficher les tâches faites
    const taskIsCompleted = allTasks.filter((eachTask) => {
        return eachTask.isComplete == true;
    });
    console.log('Tasks completed only :', taskIsCompleted)

    // Afficher les tâches à faire
    const taskToBeDone = allTasks.filter((eachTask) => {
        return eachTask.isComplete == false;
    });
    console.log('Tasks to be done :', taskToBeDone)
